How to proceed ?

1. Modify LaTeX
2. Modify references.bib if required
3. To reflect the references, you will need to execute following (either in windows powershell or on linux)

> pdflatex main; bibtex main; pdflatex main; pdflatex main;

4. Done