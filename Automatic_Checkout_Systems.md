# Automatic Checkout Systems

## Questions :

1. Currently used checkout methodologies ? How many of them are automated ?
2. Are there any shortcomings of these systems ? 
3. Can the systems be improved ? If so, how ?, If no, why ?

### IEEE Xplore Digital Library Search Results

**Keyword** : Automatic Checkout Systems

**Filters** : Years 2000-2018

**#Results** : 20

​		Journals and Magazines (1)

​		Conferences (18)

​		Courses(1)

**Notes** :

Many of the results above are due to Aerospace which are of no use to us.

(TODO: Refer back to the references of relevant papers below)

**Relevant Papers**:
<details>
<summary markdown="1">
[Auto-checkout system for retails using Radio Frequency Identification (RFID) technology](https://ieeexplore.ieee.org/document/5991855/)
</summary>
<div markdown="1">
[M. F. M. Busu ](https://ieeexplore.ieee.org/author/37945479100); [I. Ismail ](https://ieeexplore.ieee.org/author/37306326200); [M. F. Saaid ](https://ieeexplore.ieee.org/author/37322420500); [S. M. Norzeli](https://ieeexplore.ieee.org/author/37945478400)

2011 IEEE Control and System Graduate Research Colloquium

Year: 2011

Page s: 193 - 196
</div>
</details>
<details>
<summary markdown="1">
[An intelligent RFID checkout for stores](https://ieeexplore.ieee.org/document/6177362/)
</summary>
<div markdown="1">
[M. A. Besbes ](https://ieeexplore.ieee.org/author/38241860200); [H. Hamam](https://ieeexplore.ieee.org/author/37300504900)

ICM 2011 Proceeding

Year: 2011

Page s: 1 - 12
</div>
</details>
<details>
<summary markdown="1">
[A smart catering system base on Internet-of-things technique](https://ieeexplore.ieee.org/document/7399875/) 
</summary>
<div markdown="1">
[Lei Zhou](https://ieeexplore.ieee.org/author/37085618839);	[Aichuan Wang](https://ieeexplore.ieee.org/author/37085628400) ;[Yongxiang Zhang](https://ieeexplore.ieee.org/author/37085625252) ;	[Suodong Sun](https://ieeexplore.ieee.org/author/37085612017) 				 				 				 			 		

2015 IEEE 16th International Conference on Communication Technology (ICCT)

Year: 2015 				 				 					 					 				 				 			

Page s: 433 - 436
</div>
</details>
<details>
<summary markdown="1">
[Models for Cost-Benefit Analysis of RFID Implementations in Retail Stores](https://ieeexplore.ieee.org/document/4378256/)
</summary>
<div markdown="1">
[Jayavel Sounderpandian](https://ieeexplore.ieee.org/author/37946529200) ;[Rajendra V. Boppana](https://ieeexplore.ieee.org/author/37265575700) ;[Suresh Chalasani](https://ieeexplore.ieee.org/author/37295575500);[Asad M. Madni](https://ieeexplore.ieee.org/author/37294567200) 				 				 				 			 		

IEEE Systems Journal

Year: 2007 , Volume: ,[Issue:2](https://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=4383011)

Page s: 105 - 114 				 			
</div>
</details>
<details>
<summary markdown="1">
[Design of an Ultra Low-Power RFID Baseband Processor Featuring an AES Cryptography Engine](https://ieeexplore.ieee.org/document/4669322/) 
</summary>
<div markdown="1">
[Andrea Ricci](https://ieeexplore.ieee.org/author/37285585800) ;[Matteo Grisanti ](https://ieeexplore.ieee.org/author/37869982900);Ilaria De Munari ;[Paolo Ciampolini ](https://ieeexplore.ieee.org/author/37269290800)

2008 11th EUROMICRO Conference on Digital System Design Architectures, Methods and Tools

Year: 2008 				 				 					 					 				 				 			

Page s: 831 - 838 
</div>
</details>

<hr>

**Keyword** : Retail Product Checkout

**Filters** : Years 2000-2018

**#Results** : 13

​		Journals and Magazines (3)

​		Conferences (10)

**Notes** : These keywords are better suited for finding out answers of our questions

**Relevant Papers** : 
<details>
<summary markdown="1">
[An intelligent self-checkout system for smart retail](https://ieeexplore.ieee.org/document/7551621/)
</summary>
<div markdown="1">
[Bing-Fei Wu ](https://ieeexplore.ieee.org/author/37275267600); [Wan-Ju Tseng ](https://ieeexplore.ieee.org/author/37085840849); [Yung-Shin Chen ](https://ieeexplore.ieee.org/author/37085843170); [Shih-Jhe Yao ](https://ieeexplore.ieee.org/author/37085835262); [Po-Ju Chang](https://ieeexplore.ieee.org/author/37085845653)

2016 International Conference on System Science and Engineering (ICSSE)

Year: 2016

Page s: 1 - 4
</div>
</details>
<details>
<summary markdown="1">
[Visual item verification for fraud prevention in retail self-checkout](https://ieeexplore.ieee.org/document/5711557/)
</summary>
</div markdown="1">
[Russell Bobbit ](https://ieeexplore.ieee.org/author/37846190500); [Jonathan Connell ](https://ieeexplore.ieee.org/author/37269977900); [Norman Haas ](https://ieeexplore.ieee.org/author/37283429800); [Charles Otto ](https://ieeexplore.ieee.org/author/37845401100); [Sharath Pankanti ](https://ieeexplore.ieee.org/author/37269971500); [Jason Payne](https://ieeexplore.ieee.org/author/37850344800)

2011 IEEE Workshop on Applications of Computer Vision (WACV)

Year: 2011

Page s: 585 - 590
</div>
</details>
<details>
<summary markdown="1">
[NFC-enabled decentralized checkout system](https://ieeexplore.ieee.org/document/7128960/)
</summary>
<div markdown="1">
[Orlando Volpato Filho ](https://ieeexplore.ieee.org/author/37085492760); [Fabio Piva](https://ieeexplore.ieee.org/author/37085483464)

2014 IEEE Brasil RFID

Year: 2014

Page s: 35 - 37
</div>
</details>
<details>
<summary markdown="1">
[Models for Cost-Benefit Analysis of RFID Implementations in Retail Stores](https://ieeexplore.ieee.org/document/4378256/)
</summary>
<div markdown="1">
[Jayavel Sounderpandian ](https://ieeexplore.ieee.org/author/37946529200); [Rajendra V. Boppana ](https://ieeexplore.ieee.org/author/37265575700); [Suresh Chalasani ](https://ieeexplore.ieee.org/author/37295575500); [Asad M. Madni](https://ieeexplore.ieee.org/author/37294567200)

IEEE Systems Journal

Year: 2007 , Volume: 1 , [Issue: 2](https://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=4383011)

Page s: 105 - 114
</div>
</details>
<details>
<summary markdown="1">
[Enabling RFID in retail](https://ieeexplore.ieee.org/document/1607945/)
</summary>
<div markdown="1">
[G. Roussos](https://ieeexplore.ieee.org/author/37297438000)

Computer

Year: 2006 , Volume: 39 , [Issue: 3](https://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=33768)

Page s: 25 - 30
</div>
</details>


### ScienceDirect

**Keyword** : Automatic Checkout Systems

**Filters** : Years 2000-2018

**#Results** : 884

​	Research Article(425)
	
​	Review Article(34)
	
​	Books Chapter(297)
	
​	Others(128)

**Relevant Papers**:
<details>
<summary markdown="1">
[Exploring the adoption of self-service checkouts and the associated social obligations of shopping practices](https://www.sciencedirect.com/science/article/pii/S0969698917307348)
</summary>
<div markdown="1">
[Sandy Bulmer ](); [Jonathan Elms ](); [Simon Moore ]();

Journal of Retailing and Consumer Services

Pages 107-116

</div>
</details>
<details>
<summary markdown="1">
[Supermarket self-checkout service quality, customer satisfaction, and loyalty: Empirical evidence from an emerging market](https://www.sciencedirect.com/science/article/pii/S0969698913000829)
</summary>
<div markdown="1">
[FatmaDemirci Orel ](); [Ali Kara ]()

Journal of Retailing and Consumer Services

Pages 118-129

</div>
</details>
<details>
<summary markdown="1">
[Automated Classification to Improve the Efficiency of Weeding Library Collections](https://www.sciencedirect.com/science/article/abs/pii/S0099133317304160) 
</summary>
<div markdown="1">
[Kiri L.Wagstaff]();[Geoffrey Z.Liu]();		 		

The Journal of Academic Librarianship

Volume 44, Issue 2, March 2018, 

Pages 238-247

</div>
</details>
<details>
<summary markdown="1">
[RFID systems in libraries: An empirical examination of factors affecting system use and user satisfaction](https://www.sciencedirect.com/science/article/pii/S0268401212001302)
</summary>
<div markdown="1">
[Yogesh K.Dwivedi]() ;[Kawal Kapoor]() ;[Michael D. Williams]();[Janet Williams]() 				 				 				 			 		
International Journal of Information Management

Volume 33, Issue 2, April 2013, 

Pages 367-377

</div>
</details>
<details>
<summary markdown="1">
[Developing a framework to improve virtual shopping in digital malls with intelligent self-service systems](https://www.sciencedirect.com/science/article/pii/S0969698914000253) 
</summary>
<div markdown="1">
[HalukDemirkan]() ;[Jim Clinton Spohrer ]()

Journal of Retailing and Consumer Services

Volume 21, Issue 5, September 2014, 

Pages 860-868

</div>
</details>

<hr>

**Keyword** : Retail Product Checkout

**Filters** : Years 2000-2018

**#Results** : 1048

​	Research Article(686)
		
​	Review Article(28)
		
​	Books Chapter(252)
		
​	Others(82)


**Relevant Papers** : 
<details>
<summary markdown="1">
[Healthy Checkout Lines: A Study in Urban Supermarkets](https://www.sciencedirect.com/science/article/abs/pii/S1499404617300702)
</summary>
<div markdown="1">
[Tamar Adjoian MPH ](); [Rachel Dannefer ](); [Craig Willingham  ](); [Chantelle Brathwaite  ](); [Sharraine Franklin ]()

Journal of Nutrition Education and Behavior

Volume 49, Issue 8, September 2017, 

Pages 615-622.e1
</div>
</details>


### ACM 