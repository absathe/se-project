## An Evaluation of Self-Checkout Systems 

### Source : Research Gate	

Abstract : The number of self-checkout systems in stores has increased exponentially in the past 5 years becoming a common way for consumers to complete their purchases. Little research has been conducted in this pervasive area, however, other than market research studies. An online survey found that although individuals use self-checkout systems a great deal, there are a number of issues and concerns with these systems. The most cited difficulties that arise when using self-checkout systems included the barcode not scanning as well as slow customers ahead in the line. These findings including others noted throughout this paper highlight a number of opportunities for human factors professionals to make the design of self-checkout systems more user-friendly.

Key points :

Frequency counts and percentages of self-checkout usability issues as reported by students and non-students (Table 1)

Results and Better ideas

Comments :

The current study identified that consumers use these systems, primarily, when there is a long line in the traditional lane and/or when they have a few items to purchase.With respect to system delays, it would be of interest to examine the effects on user experience with varying system delays to better identify an affectively “comfortable” delay. 

Pros :

Detailed Error handling and Recovery technique

Complete analysis of Problems

Cons :

Hardware failure

Questions :

How will this affect the current jobs ?

How do you plan to handle hardware issues ? 

How difficult is to add single product ?

Conclusion -- Despite the limitations of the methodology used in this particular study, the findings revealed a number of opportunities in which human factors professionals and manufacturers can become more involved in identifying ways in which self-checkout designs and physical layouts can be improved to take into account the consumers needs and preferences. 